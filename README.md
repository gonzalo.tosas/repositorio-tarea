# Nombre de la app: Port-ON


Esta es una app diseñada para trabajadores del sector metalurgico, mas especificamente para fabricantes de portones.
Basicamente su funcion consta de recoger datos como por ejemplo metros de ancho y metros de largo del porton, material del revestimiento del mismo
y elegir si lleva motor o no. En base a esta informacion, se calculará el precio final del porton y la cantidad de materiales que se van a necesitar
para confeccionarlo.
